<?php

namespace App;

use Elasticsearch\ClientBuilder;

class Es {
  private $host;
  private $port;
  private $clientBuilder;
  private $client;

  public function __construct($host = 'localhost', $port = 9200) {
    $this->host = $host;
    $this->port = $port;

    $config = [ 'host' => $this->host . ":". $this->port ];
    $this->clientBuilder = ClientBuilder::create();
    $this->clientBuilder = $this->clientBuilder->setHosts($config);
    $this->client = $this->clientBuilder->build();
  }

  public function get($query) {
    if(!isset($query)) {
      echo "Query is mandatory.";
      die;
    }

    return $this->client;
  }

  public function search($query) {
    if(!isset($query)) {
      echo "Query search is mandatory.";
    }

    return json_encode($this->client->search([
      'index' => 'benchmarks_index',
      'type' => 'doc',
      'body' => $query]));
  }

  public function post($data) {
    
    if(!isset($data)) {
      echo "Query search is mandatory.";
    }

    $params = [
    'index' => 'zeepos_transaction',
    'type' => 'zeepos_transaction',
    ];

    try {

      foreach($data as $trx_index => $transaction) 
      {
        foreach ($transaction as $i => $v) {
          $params['id'] = $v['sales_item_id'];
          unset($v['sales_item_id']);
          $params['body'] = $v;
          $response[] = $this->client->index($params);
        }
      }
    } catch(Exception $e) {
      return $e->getMessage();
    } catch(\Elasticsearch\Common\Exceptions\BadRequest400Exception $e) {
      return $e->getMessage();
    }

    return json_encode($response);
  }

}
