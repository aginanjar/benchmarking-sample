<?php
require_once __DIR__.'/vendor/autoload.php';

use App\Es;
use App\MySql;
use function Functional\group;

define('ES_PORT', 9200);
define('ES_HOST', '127.0.0.1'); // Container accessing host machine, for mac


// MySql get raw query
$pdo = new MySql(
  'localhost',
  'db_name',  
  'username',
  'password'
);

// Use your custom query
$query = "SELECT * FROM users" ;
// -- end here

$data = $pdo->get($query);
$trx_grouped = array_group_by($data, 'receipt_no');
echo '<pre />';

$arr_index = 0;
foreach ($trx_grouped as $transaction_key => $transaction) {
    foreach($transaction as $item_key => $item) {
        if($item['payment_id'] == '' || $item['payment_id'] == NULL)
        {
            $result[$arr_index][$item_key]['sales_item_id'] = $item['sales_item_id'];
            $result[$arr_index][$item_key]['product'] = [
                'name'=> $item['item_name'],
                'department_name' => $item['department_name'],
                'group_name' => $item['product_group_name']
            ];
            $result[$arr_index][$item_key]['time']= date('j/n/Y', $item['business_session_date']);
            $result[$arr_index][$item_key]['period']= [
                'HOD'=>date('G', $item['business_session_date']),
                'DOW'=>date('l', $item['business_session_date']),
                'DOM'=>date('j', $item['business_session_date']),
                'MOY'=> date('n', $item['business_session_date']),
            ];

            $result[$arr_index][$item_key]['outlet']=[
                'name' => $item['account_name'],
                'user' => $item['full_name'],
                'email' => $item['email']
            ];

            $result[$arr_index][$item_key]['transaction_id']= $item['receipt_no'];
            $result[$arr_index][$item_key]['item_count']= $item['item_quantity'];
            $result[$arr_index][$item_key]['subtotal']= $item['item_quantity'] * $item['price_shift'];

            $result[$arr_index][$item_key]['discount'] = [];
            if($item['item_type'] == 3 && $item['promotion_id'] != '') // discount bill
            {
                $result[$arr_index][$item_key]['discount'][$item['promotion_name']] = $item['total_bill_promotion'];
            }
            if($item['item_type'] == 1 && $item['promotion_id'] != '') // discount item
            {
                $result[$arr_index][$item_key]['discount'][$item['promotion_name']] = $item['discount'];
            }

            $result[$arr_index][$item_key]['tax']= $item['tax_amount'];
            $result[$arr_index][$item_key]['charges']=$item['total_charge_tax'];
            
            $result[$arr_index][$item_key]['payment']= [
                $item['payment_name'] => $item['payment_amount']
            ];

            $result[$arr_index][$item_key]['refund']= 0 ;
            if($item['transaction_type'] == 3 || $item['transaction_type'] == 4) {
                $result[$arr_index][$item_key]['refund']=  $item['price_shift'];
                $result[$arr_index][$item_key]['refund_qty']= $item['item_quantity'];
            }

            $result[$arr_index][$item_key]['timestamp'] =  date(DATE_ISO8601, $item['business_session_date'] );
        }
    }
    $arr_index++;
}

$result = array_values($result);

// Debug
// print_r($result);die;


// Es connector
$es = new Es(ES_HOST,ES_PORT);
/**
*
* Example search using query Elasticsearch
* $query = (array) json_decode('{"aggs":{"group_by_status":{"terms":{"field":"map.status","size":5,"order":{"qty":"desc","price":"asc"}},"aggs":{"qty":{"sum":{"field":"map.quantity"}},"price":{"avg":{"field":"map.price"}}}}},"size":0}');
* echo $es->search($query);
*
*/



/**
 * Example post / sync data from mysql to 
 */
$time_start = microtime(true); 
echo $es->post($result);
$time_end = microtime(true);
$execution_time = ($time_end - $time_start)/60;
//execution time of the script

echo '<br /><b>Total Execution Time:</b> '.$execution_time.' Mins';