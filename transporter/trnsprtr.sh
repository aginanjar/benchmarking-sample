#!/bin/sh
docker run --rm -v $(pwd)/transporter:/usr/src/transporter \
-w /usr/src/transporter \
quay.io/compose/transporter:latest transporter $@
