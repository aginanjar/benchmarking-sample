let constraint = 10000000;
// Synchronous
// for (let i = 1; i < constraint; i++) {
//     console.log(i);
// }

// Promises
// const test = () => {
//     return new Promise((resolve, reject) => {
//         let res = [];
//         try {
//             for (let i = 1; i < constraint; i++) {
//                 res.push(i);
//             }
//             resolve(res);
//         } catch (err) {
//             reject(err);
//         }
//     });
// };

// const result =  test();
// result.then((success) => {
//     console.log(success);
// }, (err) => {
//     console.log(err);
// });

// Async
const test2 = async () => {
    let res = [];
    for (let i = 1; i < constraint; i++) {
        res.push(i);
    }

    return await res;
};

// const display = async (res) => {
//     return res;
// };
// let rest = display(test2);
let rest = test2();
rest.then(function(s) {
    console.log(s);
});
// console.log( rest );