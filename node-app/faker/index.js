const faker = require('faker');
const mongoose = require('mongoose');

let db = async () => {
    mongoose
    .connect(
        'mongodb://mongo:27017/sample',
        { useNewUrlParser: true }
    )
    .then( async () => {
        console.log('MongoDB Connected');
        await mongoose.connection.close(() => console.log(`MongoDB connection is closed when everything is done.`));
    })
    .catch(err => console.log(`Message: ${err}`))
};


let ItemSchema = new mongoose.Schema({
    name: String,
    quantity: Number,
    price: Number,
    created_at: Date,
    status: Boolean,
});

let Item = mongoose.model('item', ItemSchema);
let list = [];
let limit = 10000;

const newItem = async (collection) => {

    try {
        
        let name = faker.commerce.productName();
        let quantity = faker.random.number();
        let price = faker.commerce.price();
        let created_at = faker.date.past();
        let status = faker.random.boolean();

        let values = [];

        for (let index = 0; index < limit; index++) {

            values.push({
                name: name,
                quantity: quantity,
                price: price,
                created_at: created_at,
                status: status,
            });
        }

        return await collection.insertMany(values,(err, docs) => {
            if(err) console.log(err);
            
            console.log(`${docs.length} successfully added mongodb.`);
        });

    } catch(error) {
        console.log(error);
    }
};

setInterval(() => {
    db();

    newItem(Item)
}, 30000);
