const mongoose = require('mongoose');

var t0 = new Date().getTime();

// DB Connection
let db = async () => {
    mongoose
        .connect(
            'mongodb://localhost:27018/sample',
            { useNewUrlParser: true }
        )
        .then(async () => {
            console.log('MongoDB Connected');
            await mongoose.connection.close(() => {});
        })
        .catch(err => console.log(`Message: ${err}`))
};

// Schema
let ItemSchema = new mongoose.Schema({
    name: String,
    quantity: Number,
    price: Number,
    created_at: Date,
    status: Boolean,
});
let Item = mongoose.model('item', ItemSchema);

// Get Aggregate method
const getAggregate = async (collection) => {

    try {
        return await collection.aggregate(
            [
                {
                    $group:
                    {
                        _id: { status: "$status" },
                        totalQuantity: { $sum: "$quantity" },
                        averagePrice: { $avg: { $multiply: ["$price", "$quantity"] } },
                        count: { $sum: 1 }
                    }
                }
            ]
        );
    } catch (error) {
        console.log(error);
    }
};

db();
getAggregate(Item)
    .then(success=> {
        console.log(success);
        var t1 = new Date().getTime();
        console.log("Took: " + (t1 - t0) + " milliseconds.")
    })
    .catch(err=>console.log(err));

/**
 ---------------------------------------------------------------
/usr/src/app # time node index.js
MongoDB Connected
MongoDB connection is closed when everything is done.
[ { _id: { status: true },
    totalQuantity: 126356719390,
    averagePrice: 24109798.467944603,
    count: 2628559 },
  { _id: { status: false },
    totalQuantity: 115802612528,
    averagePrice: 25164617.956354577,
    count: 2390285 } ]
real    0m 11.30s
user    0m 1.12s
sys     0m 1.16s
---------------------------------------------------------------
*/