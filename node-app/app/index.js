const http = require('http');
const es = require('./getAggregateElastic.js');

// Init processing time
let t0 = new Date().getTime();

// App configs
let PORT = 3000;
let success_code = 200;

// Search
  let input = {
    "index": "benchmarks_index",
    "type": "doc",
    "body": {"aggs":{"group_by_status":{"terms":{"field":"map.status","size":5,"order":{"qty":"desc","price":"asc"}},"aggs":{"qty":{"sum":{"field":"map.quantity"}},"price":{"avg":{"field":"map.price"}}}}},"size":0}
  };


http.createServer((request, response) => {
  response.writeHead(success_code, {
    'Content-Type':'application/json'
  });

  es.search(input, response);
  
  let t1 = new Date().getTime();
  console.log("Took: " + (t1 - t0) + " milliseconds.")

}).listen(PORT);
