const elasticsearch = require('elasticsearch');
const client = new elasticsearch.Client({
  host: 'docker.for.mac.host.internal:9200'
});

module.exports = {
  search: (request, serverResponse) => {
    return client.search(request)
      .then(data => {
        serverResponse.write(JSON.stringify(data));
        serverResponse.end();
      })
      .catch(err => console.log(err));
    }
  
}
